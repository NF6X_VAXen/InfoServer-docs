InfoServer, SYSTEM_MGMT, InfoServer Software

This directory contains various InfoServer software disk images, the
client for Microsoft MS-DOS (the OpenVMS LAD/LAST client is built into
OpenVMS), and the associated keys for enabling various InfoServer
functions.

InfoServer devices are early network server devices, and provide
various functions in support of OpenVMS itself (including network
bootstrap and installation of OpenVMS), and in support of other
systems with InfoServer clients.

To use these disk images, these images must be replicated onto floppy
disk media (the MS-DOS kit), or onto CD-R media, and then loaded into
an appropriate MS-DOS client, or onto an InfoServer device.

As an alternative to InfoServer hardware, systems running OpenVMS I64
V8.2-1 and later, and OpenVMS Alpha V8.3 and later, include host-based
InfoServer server support, and directly integrated into OpenVMS itself.
This in addition to the long-standing LAD/LAST client capabilities, and
the bootp/tftp support required for downloading OpenVMS I64 into an HP
Integrity server.

InfoServer MS-DOS V2.0 client floppy disk images
ak-plz3c-ca_1-of-3.img
ak-plz3c-ca_2-of-3.img
ak-plz3c-ca_3-of-3.img

VTX Software V2.1
InfoServer/VMS
ag-pjjhh-be.img

InfoServer/VMS V3.5
ag-r7lfa-bs.img

InfoServer CDR Function (Open Access)
ag-q1mua-xe.img

InfoServer 1000 Tape Function (Open Access)
ag-pybja-xe.img

InfoServer 100/150 Tape Function (Open Access)
ag-pjxla-re.img

InfoServer 100/150 Disk Function (Open Access)
ag-pjxka-re.img

--

LAD: Local Area Disk, a disk storage protocol used by InfoServer devices,
and supported by the consoles of various MicroVAX, VAX systems, and by
the SRM console of Alpha systems.

LAST: Local Area Tape.  Not to be confused with Local Area Terminal (LAT).
